package com.epstim.filtertest.filtertest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.Date;


public class Main extends Activity implements SeekBar.OnSeekBarChangeListener {
    private static final int OPEN_IMAGE = 1;
    private static final int OPEN_OVERLAY = 2;
    SeekBar redBar, greenBar, blueBar, redTranslate, greenTranslate, blueTranslate, rb, gb, bg, ri, gi, bi;
    TextView redText, greenText, blueText, redTranslateText, greenTranslateText, blueTranslateText, rbText, gbText, bgText, riText, biText, giText;
    ImageView imageView;
    Context context;
    Bitmap background;
    Bitmap overlay = null;

    float[] colorMatrix = {
            1, 0, 0, 0, 0,
            0, 1, 0, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 0, 1, 0
    };

    private void setDefaultImages() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = true;
        options.inSampleSize = 4;

        background = BitmapFactory.decodeResource(getResources(), R.drawable.image2, options);
        overlay = null;
        Paint paint = new Paint();
        paint.setColorFilter(matrixToFilter());
        imageView.setImageBitmap(ImageHelper.getMergedBitmaps(background, overlay, paint));
    }

    private void setDefaultColorMatrix() {
        colorMatrix = new float[]{
                1, 0, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 0, 1, 0
        };
    }

    private void resetSeekBars() {
        redBar.setProgress(510);
        greenBar.setProgress(510);
        blueBar.setProgress(510);
        redTranslate.setProgress(255);
        greenTranslate.setProgress(255);
        blueTranslate.setProgress(255);
        rb.setProgress(255);
        gb.setProgress(255);
        bg.setProgress(255);
        ri.setProgress(255);
        gi.setProgress(255);
        bi.setProgress(255);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();

        redBar = (SeekBar) findViewById(R.id.redBar);
        redBar.setOnSeekBarChangeListener(this);
        redTranslate = (SeekBar) findViewById(R.id.redTranslate);
        redTranslate.setOnSeekBarChangeListener(this);
        rb = (SeekBar) findViewById(R.id.rb);
        rb.setOnSeekBarChangeListener(this);
        ri = (SeekBar) findViewById(R.id.ri);
        ri.setOnSeekBarChangeListener(this);

        greenBar = (SeekBar) findViewById(R.id.greenbar);
        greenBar.setOnSeekBarChangeListener(this);
        greenTranslate = (SeekBar) findViewById(R.id.greenTranslate);
        greenTranslate.setOnSeekBarChangeListener(this);
        gb = (SeekBar) findViewById(R.id.gb);
        gb.setOnSeekBarChangeListener(this);
        gi = (SeekBar) findViewById(R.id.gi);
        gi.setOnSeekBarChangeListener(this);

        blueBar = (SeekBar) findViewById(R.id.blueBar);
        blueBar.setOnSeekBarChangeListener(this);
        blueTranslate = (SeekBar) findViewById(R.id.blueTranslate);
        blueTranslate.setOnSeekBarChangeListener(this);
        bg = (SeekBar) findViewById(R.id.bg);
        bg.setOnSeekBarChangeListener(this);
        bi = (SeekBar) findViewById(R.id.bi);
        bi.setOnSeekBarChangeListener(this);

        redText = (TextView) findViewById(R.id.redText);
        redText.setText(String.valueOf(1));
        redTranslateText = (TextView) findViewById(R.id.redTranslateText);
        rbText = (TextView) findViewById(R.id.rbText);
        riText = (TextView) findViewById(R.id.riText);

        greenText = (TextView) findViewById(R.id.greenText);
        greenText.setText(String.valueOf(1));
        greenTranslateText = (TextView) findViewById(R.id.greenTranslateText);
        gbText = (TextView) findViewById(R.id.gbText);
        giText = (TextView) findViewById(R.id.giText);

        blueText = (TextView) findViewById(R.id.blueText);
        blueText.setText(String.valueOf(1));
        blueTranslateText = (TextView) findViewById(R.id.blueTranslateText);
        bgText = (TextView) findViewById(R.id.bgtext);
        biText = (TextView) findViewById(R.id.biText);

        imageView = (ImageView) findViewById(R.id.imageView);

        setDefaultImages();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor;
        int columnIndex;
        Paint paint = new Paint();
        paint.setColorFilter(matrixToFilter());
        String picturePath;
        switch (requestCode) {
            case OPEN_IMAGE:
                if (resultCode != Activity.RESULT_OK) break;
                uri = data.getData();
                cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
                background = BitmapFactory.decodeFile(picturePath);
                break;
            case OPEN_OVERLAY:
                if (resultCode != Activity.RESULT_OK) break;
                uri = data.getData();
                cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
                overlay = BitmapFactory.decodeFile(picturePath);
                break;
            default:
                setDefaultImages();
                break;
        }
        imageView.setImageBitmap(ImageHelper.getMergedBitmaps(background, overlay, paint));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        int id = item.getItemId();
        switch (id) {
            case R.id.save:
                new SaveAsyncTask().execute();
                break;
            case R.id.undo:
                setDefaultColorMatrix();
                resetSeekBars();
                setDefaultImages();
                imageView.setColorFilter(matrixToFilter());
                break;
            case R.id.open:
                startActivityForResult(Intent.createChooser(intent, "Select new background:"), OPEN_IMAGE);
                break;
            case R.id.addOverlay:
                startActivityForResult(Intent.createChooser(intent, "Select new overlay:"), OPEN_OVERLAY);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

        int id = seekBar.getId();
        String format = String.valueOf(new DecimalFormat("##.###").format(((double) progress - 255.0) / 255.0));
        switch (id) {
            case R.id.redBar:
                redText.setText(format);
                break;
            case R.id.greenbar:
                greenText.setText(format);
                break;
            case R.id.blueBar:
                blueText.setText(format);
                break;
            case R.id.redTranslate:
                redTranslateText.setText(format);
                break;
            case R.id.greenTranslate:
                greenTranslateText.setText(format);
                break;
            case R.id.blueTranslate:
                blueTranslateText.setText(format);
                break;
            case R.id.rb:
                rbText.setText(format);
                break;
            case R.id.gb:
                gbText.setText(format);
                break;
            case R.id.bg:
                bgText.setText(format);
                break;
            case R.id.ri:
                riText.setText(format);
                break;
            case R.id.gi:
                giText.setText(format);
                break;
            case R.id.bi:
                biText.setText(format);
                break;
            default:
                break;
        }
        colorMatrix[0] = (float) (redBar.getProgress() - 255) / 255.0f;
        colorMatrix[6] = (float) (greenBar.getProgress() - 255) / 255.0f;
        colorMatrix[12] = (float) (blueBar.getProgress() - 255) / 255.0f;
        colorMatrix[1] = (float) (redTranslate.getProgress() - 255) / 255.0f;
        colorMatrix[5] = (float) (greenTranslate.getProgress() - 255) / 255.0f;
        colorMatrix[10] = (float) (blueTranslate.getProgress() - 255) / 255.0f;
        colorMatrix[2] = (float) (rb.getProgress() - 255) / 255.0f;
        colorMatrix[7] = (float) (gb.getProgress() - 255) / 255.0f;
        colorMatrix[11] = (float) (bg.getProgress() - 255) / 255.0f;
        colorMatrix[3] = (float) (ri.getProgress() - 255) / 255.0f;
        colorMatrix[8] = (float) (gi.getProgress() - 255) / 255.0f;
        colorMatrix[13] = (float) (bi.getProgress() - 255) / 255.0f;
        imageView.setColorFilter(matrixToFilter());
    }

    private ColorMatrixColorFilter matrixToFilter() {
        return new ColorMatrixColorFilter(colorMatrix);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private class SaveAsyncTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog = new ProgressDialog(Main.this);

        @Override
        protected void onPreExecute() {
            dialog.setTitle("Saving");
            dialog.setMessage("Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            Toast.makeText(getApplicationContext(), "Saving succeed!", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            OutputStream fOut;
            try {
                File root = new File(Environment.getExternalStorageDirectory() + File.separator + "FilterTest" + File.separator);
                String timestamp = String.valueOf(new Date().getTime());
                File sdImageMainDirectory = new File(root, "Filter_" + timestamp + ".jpg");
                File sdSettingsMainDirectory = new File(root, "Setting_" + timestamp + ".txt");
                fOut = new FileOutputStream(sdImageMainDirectory);

                Bitmap bm1 = BitmapFactory.decodeResource(getResources(), R.drawable.image2);
                Paint paint = new Paint();
                paint.setColorFilter(matrixToFilter());

                ImageHelper.getMergedBitmaps(bm1, overlay, paint).compress(Bitmap.CompressFormat.JPEG, 100, fOut);

                fOut.flush();
                fOut.close();
                fOut = new FileOutputStream(sdSettingsMainDirectory);
                PrintStream ps = new PrintStream(fOut);
                ps.print("{ ");
                for (int i = 0; i < colorMatrix.length; i++) {
                    if ((i % 5 == 0) && (i != 0)) ps.println();
                    if (i != 19) ps.print(String.valueOf(colorMatrix[i]) + "f,\t");
                    if (i == 19) ps.print(String.valueOf(colorMatrix[i]) + "f\t");
                }
                ps.println();
                ps.print(" };");
                ps.flush();
                ps.close();
                fOut.flush();
                fOut.close();
            } catch (Exception ex) {
                Toast.makeText(getApplicationContext(), "Saving Failed!", Toast.LENGTH_SHORT).show();
                Log.d("Main", "Save image to file exception: " + ex.getLocalizedMessage());
                ex.printStackTrace();
            }
            return null;
        }
    }
}
