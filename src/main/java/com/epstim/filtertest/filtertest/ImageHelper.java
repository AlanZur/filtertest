package com.epstim.filtertest.filtertest;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Envy on 2014-04-29.
 */
public class ImageHelper {
    public static final String TAG = ImageHelper.class.getSimpleName();

    public static Bitmap getMergedBitmaps(Bitmap source, Bitmap overlay, Paint paint) {
        Bitmap out = null;
        if (overlay != null) {
            overlay = Bitmap.createScaledBitmap(overlay, source.getWidth(), source.getHeight(), false);
            out = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(out);
            canvas.drawBitmap(source, 0, 0, paint);
            Paint opcaity = new Paint();
            opcaity.setAlpha(127);
            canvas.drawBitmap(overlay, 0, 0, opcaity);
            canvas.save();

        } else {
            out = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(out);
            canvas.drawBitmap(source, 0, 0, paint);
        }
        return out;
    }
}
